import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharatersListComponent } from './charaters-list/charaters-list.component';

const routes: Routes = [
    {
        path: '',
        component: CharatersListComponent
    },
    {
        path: '**',
        component: CharatersListComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CharactersRoutingModule { }