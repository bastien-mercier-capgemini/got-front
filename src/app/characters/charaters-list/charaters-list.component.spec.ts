import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharatersListComponent } from './charaters-list.component';

describe('CharatersListComponent', () => {
  let component: CharatersListComponent;
  let fixture: ComponentFixture<CharatersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharatersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharatersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
