import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { CharacterModel } from 'app/shared/models/character.model';
import { CharactersService } from '../characters.service';

@Component({
  selector: 'got-charaters-list',
  templateUrl: './charaters-list.component.html',
  styleUrls: ['./charaters-list.component.sass']
})
export class CharatersListComponent implements OnInit {

  characters$: Observable<CharacterModel[]>

  constructor(
    private characterService: CharactersService
  ) { }

  ngOnInit() {
    console.log('load');
    this.characters$ = this.characterService.getList(); 
  }

}
