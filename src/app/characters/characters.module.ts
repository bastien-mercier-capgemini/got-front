import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharatersListComponent } from './charaters-list/charaters-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { CharactersRoutingModule } from './characters-routing.module';
import { MatCardModule, MatCheckboxModule, MatInputModule, MatGridListModule, MatChipsModule } from '@angular/material';

@NgModule({
  declarations: [CharatersListComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule,
    MatGridListModule,
    MatChipsModule,
    CharactersRoutingModule
  ]
})
export class CharactersModule { }
