import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CharacterModel } from 'app/shared/models/character.model';

@Injectable({
    providedIn: 'root'
})
export class CharactersService {

    constructor(private http: HttpClient) {}
    
    getList(): Observable<CharacterModel[]> {
        return this.http.get<CharacterModel[]>('/api/personnage/liste');
    }

}
