import { Component } from '@angular/core';

@Component({
  selector: 'got-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent { }
