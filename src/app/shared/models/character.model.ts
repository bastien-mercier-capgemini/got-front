import { FamilyEnum } from '../enums/family.enum';

export class CharacterModel {

    id: number;
    nom: string;
    prenom: string;
    famille: FamilyEnum;
    statut: string;
}
