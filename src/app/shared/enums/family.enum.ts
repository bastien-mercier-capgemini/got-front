export enum FamilyEnum {
    
    Targaryen = 'Targaryen',
    Arryn = 'Arryn',
    Stark = 'Stark',
    Lannister = 'Lannister',
    Greyjoy = 'Greyjoy',
    Baratheon = 'Baratheon',
    Tully = 'Tully',
    Frey = 'Frey',
    Tyrrel = 'Tyrrel',
    Bolton = 'Bolton',
    Martell = 'Martell'

}